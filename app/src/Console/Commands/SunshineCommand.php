<?php

namespace JackLuhn\Console\Commands
{
	use Symfony\Component\Console\Input\InputArgument;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Input\InputOption;
	use Symfony\Component\Console\Output\OutputInterface;
	
	use JackLuhn\Console\Command;
	
	/**
	 * Author: Jack Luhn <jackluhn36@gmail.com>
	 */
	class SunshineCommand extends Command
	{
		protected static $defaultName = 'app:sunshine';
		
		public function configure()
		{
		    //$this->setName('greet');
		    $this->setDescription('Greet a user based on the time of the day.');
		    $this->setHelp('This command allows you to greet a user based on the time of the day...');
		    $this->addArgument('username', InputArgument::REQUIRED, 'The username of the user.');
		}
		public function execute(InputInterface $input, OutputInterface $output)
		{
		    $this -> greetUser($input, $output);
		}
	}
}

?>
