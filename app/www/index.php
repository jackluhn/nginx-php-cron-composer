<?php

	require(__DIR__."/../vendor/autoload.php");
	
	use Klein\Klein as Router;
	
	$router = new Router();
	
	$router->respond('/', function($request, $response, $service, $app) {
		echo "hello world";
	});

	$router->dispatch();

?>
